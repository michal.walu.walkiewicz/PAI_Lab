<?php
//Michał Walkiewicz 17768 formularz logowania
session_start();
if(isset($_POST['submit'])) {
    $login = $_POST['login'] ?? '';
    $password = $_POST['password'] ?? '';
    $accepted_only_login = 'admin';
    $accepted_only_pass = 'admin';
    if ($login == $accepted_only_login && $password == $accepted_only_pass) {
        $_SESSION['user_zalogowany'] = true;
        header("Location: tekst.php");
        exit();
    }
    else
    {
        echo '<script type="text/javascript"> ';
        echo 'alert("Podano nieprawidłowe dane logowania. Spróbuj ponownie.\nHint: admin admin ");';
        echo '</script>';
    }
}
?>

<!doctype html>
<html lang="en">
<!--Michał Walkiewicz 17768 formularz logowania-->
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Formularz logowania</title>
</head>
<body>
<form action="" method="post">
    <input type="text" name="login" placeholder="Login">
    <br>
    <input type="text" name="password" placeholder="Hasło">
    <br>
    <button type="submit" name="submit">Zaloguj</button>
</form>
</body>
</html>

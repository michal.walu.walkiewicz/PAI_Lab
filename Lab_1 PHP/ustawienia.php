<?php
//Michał Walkiewicz 17768 formularz ustawień
session_start();
if (empty($_SESSION['user_zalogowany'])) {
    header("Location: form_logowanie.php");
    exit();
}
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8" />
	<style type="text/css">
		ul#menu {
			float: left;
			width: 10%;
			list-style-type: none;
		}
		div#tresc {
			float: left;
			width: 85%;

		}

		div#tresc table span {
			padding: 3px;
		}
	</style>
</head>

<body>
<form action="" method="post">
	<ul id="menu">
		<li><a href="tekst.php">Tekst</a></li>
		<li><a href="ustawienia.php">Ustawienia</a></li>
	</ul>
	<div id="tresc">
		<table cellpadding="4">
			<tr>
				<td>Kolor tla strony:</td>
				<td>
					<input type="radio" name="wybrany_kolor_tla" value="#BAFF49" <?php if(isset($_SESSION['wybrany_kolor_tla']) && $_SESSION['wybrany_kolor_tla'] == '#BAFF49'){echo("checked");}?>/> <span style="background-color: #BAFF49">#BAFF49</span><br />
					<input type="radio" name="wybrany_kolor_tla" value="#8E9BFF" <?php if(isset($_SESSION['wybrany_kolor_tla']) && $_SESSION['wybrany_kolor_tla'] == '#8E9BFF'){echo("checked");}?>/> <span style="background-color: #8E9BFF">#8E9BFF</span><br />
					<input type="radio" name="wybrany_kolor_tla" value="#FFEFBF" <?php if(isset($_SESSION['wybrany_kolor_tla']) && $_SESSION['wybrany_kolor_tla'] == '#FFEFBF'){echo("checked");}?>/> <span style="background-color: #FFEFBF">#FFEFBF</span><br />
				</td>
			</tr>
			<tr>
				<td>Krój czcionki:</td>
				<td>

					<select name="kroj_czcionki">
						<option value="Verdana" <?php if(isset($_SESSION['kroj_czcionki']) && $_SESSION['kroj_czcionki'] == 'Verdana'){echo("selected");}?>/>Verdana</option>
						<option value="Arial" <?php if(isset($_SESSION['kroj_czcionki']) && $_SESSION['kroj_czcionki'] == 'Arial'){echo("selected");}?>/>Arial</option>
						<option value="Courier New" <?php if(isset($_SESSION['kroj_czcionki']) && $_SESSION['kroj_czcionki'] == 'Courier New'){echo("selected");}?>/>Courier New</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Wielkość czcionki:</td>
				<td><input type="text" name="liczba" style="width: 30px" value="<?=$_SESSION['liczba'] ?? '' ?>"/> px</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>
                    <input type="submit" value="Zapisz" name="submited"/>
                </td>
			</tr>
		</table>
	</div>
</form>
    <?php
    if(isset($_POST['submited']))
    {
            if(isset($_POST['wybrany_kolor_tla']))
                $_SESSION['wybrany_kolor_tla'] = $_POST['wybrany_kolor_tla'];
            if(isset($_POST['kroj_czcionki']))
                $_SESSION['kroj_czcionki'] = $_POST['kroj_czcionki'];
            if(isset($_POST['liczba']))
                $_SESSION['liczba'] = $_POST['liczba'];
            header("Refresh:0");
    }
    ?>
</body>

</html>